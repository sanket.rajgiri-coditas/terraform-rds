locals {
  tag = {
    Environment = contains(keys(var.rds_conf), "env") ? var.rds_conf.env : var.env
  }
  region   = contains(keys(var.rds_conf), "region") ? var.rds_conf.region : var.region
  rds_conf = merge(var.default_rds_conf, var.rds_conf)
  tags     = contains(keys(var.rds_conf), "tags") ? merge(var.rds_conf.tags, local.tag) : local.tag
}
